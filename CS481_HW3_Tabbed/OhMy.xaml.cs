﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW3_Tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OhMy : ContentPage
    {
        public string _tpage = "";
        public string tpage
        {
            get
            {
                return _tpage;
            }
            set
            {
                _tpage = value;
                OnPropertyChanged("tpage");
            }
        }
        public OhMy()
        {
            InitializeComponent();
        }
        protected override void OnAppearing()
        {

            base.OnAppearing();
            BindingContext = this;
            var state = Preferences.Get("my_key", "default_value");
            Debug.WriteLine(state);
            if (state == "lion") tpage = "lion.jpg";
            else if (state == "bear") tpage = "bear.jpg";
            else if (state == "tiger") tpage = "tiger.gif";
        }
        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            Preferences.Set("my_key", "ohmy");
        }
    }
}