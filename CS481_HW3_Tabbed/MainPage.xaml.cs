﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CS481_HW3_Tabbed
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : TabbedPage
    {
        public int state = 0;
        public MainPage()
        {
            // Container that holds all pages
            InitializeComponent();
            Children.Add(new Lions());
            Children.Add(new Tigers());
            Children.Add(new Bears());
            Children.Add(new OhMy());

        }


    }
}
